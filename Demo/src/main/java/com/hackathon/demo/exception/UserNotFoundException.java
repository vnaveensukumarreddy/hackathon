package com.hackathon.demo.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
@NoArgsConstructor
public class UserNotFoundException extends RuntimeException {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String exception) {
        super(exception);
    }
}

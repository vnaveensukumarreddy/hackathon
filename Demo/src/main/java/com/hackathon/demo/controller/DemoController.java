package com.hackathon.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
@RestController
@RequestMapping("")
public class DemoController {

	@Operation(summary = "/info/version")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "/info/version ") })
	@GetMapping("/info/version")
	public String checkVersion() {
		return "Greetings from Spring Boot!";
	}
	
}
